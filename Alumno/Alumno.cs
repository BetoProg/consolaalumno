﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alumno
{
    public class Alumno
    {
        public string Nombre { get; set; }
        public string  Ap_Paterno { get; set; }
        public string Ap_Materno { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
        public bool Estatus { get; set; }

        public Alumno(string nombre, string ape_paterno, string ape_materno)
        {
            Nombre = nombre;
            Ap_Paterno = ape_paterno;
            Ap_Materno = ape_materno;
        }
        public Alumno(string nombre, string ape_paterno, string ape_materno, DateTime fecha_nacimento)
        {
            Nombre = nombre;
            Ap_Paterno = ape_paterno;
            Ap_Materno = ape_materno;
            Fecha_Nacimiento = fecha_nacimento;
        }

        public string ObtenerNombre()
        {
            return string.Format("Tu nombre es: {0} {1} {2} y naciste: {3}", Nombre, Ap_Paterno, Ap_Materno, Fecha_Nacimiento);
        }
    }
}
